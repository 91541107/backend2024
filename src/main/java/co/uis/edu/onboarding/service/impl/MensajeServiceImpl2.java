package co.uis.edu.onboarding.service.impl;

import co.uis.edu.onboarding.model.Mensaje;
import co.uis.edu.onboarding.repository.IMensajeRepository;
import co.uis.edu.onboarding.service.interfaces.IMensajeService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Primary
@Service
public class MensajeServiceImpl2 implements IMensajeService {

    private IMensajeRepository mensajeRepository; // asi se declara una variable de instacia llamada mensajeRepository(se usa para acceder a los datos relacionados con los mensajes) de tipp IMensajeRepository

    public MensajeServiceImpl2(IMensajeRepository mensajeRepository) {
        this.mensajeRepository = mensajeRepository;
    }

    public List<Mensaje> findAllList() { // sobre escribe el metodo findAllList de la interface IMensajeService
        return this.mensajeRepository.findAll(); // retorna una lista de objetos de la tabla mensaje
    }

}