package co.uis.edu.onboarding.service.interfaces;

import co.uis.edu.onboarding.model.Mensaje;

import java.util.List;

public interface IMensajeService {
    List<Mensaje> findAllList();
}
