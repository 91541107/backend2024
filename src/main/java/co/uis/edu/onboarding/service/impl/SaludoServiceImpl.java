package co.uis.edu.onboarding.service.impl;

import co.uis.edu.onboarding.service.interfaces.ISaludoService;
import org.springframework.stereotype.Service;

@Service
public class SaludoServiceImpl implements ISaludoService {
    @Override
    public String greetingAccordingNumber(Long id) {
        if(id!= null && id %2==0){
            return "hola, el número es par";
        }
        return "hola, el número es impar";
    }
}
