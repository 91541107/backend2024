package co.uis.edu.onboarding.service.impl;
import co.uis.edu.onboarding.service.interfaces.ISaludoService;
import org.springframework.stereotype.Service;

@Service
public class SaludoUrgenteServiceImpl implements ISaludoService {
    @Override
    public String greetingAccordingNumber(Long id) {
        return "hola, el nuúmero que escribiste es "+id+" y estamos probando bajo acomplamiento alta cohesión";
    }
}