package co.uis.edu.onboarding.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "mensajes")
public class Mensaje implements Serializable {
    private static final long serialVersionUID = -2129453057143731379L;
    @Id
    private Long id;
    @Column
    private String asunto;
    @Column(name = "mensaje_default", nullable = false)
    private String mensajeDefault;
    @Column(name = "id_emisario", nullable = false)
    private Long idEmisario;
    private Boolean activo;
    private String descripcion;
}
