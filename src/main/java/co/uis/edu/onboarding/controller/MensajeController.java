package co.uis.edu.onboarding.controller;

import co.uis.edu.onboarding.model.Mensaje;
import co.uis.edu.onboarding.service.interfaces.IMensajeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/messages")
public class MensajeController {

    private IMensajeService mensajeService;


    public MensajeController(IMensajeService mensajeService) {
        this.mensajeService = mensajeService;
    }

    /**
     * todos los mensajes disponibles
     */
    @GetMapping("/all")
    public ResponseEntity<List<Mensaje>> findAll() {
        return new ResponseEntity<>(this.mensajeService.findAllList(), HttpStatus.OK);
    }

}