package co.uis.edu.onboarding.controller;


import co.uis.edu.onboarding.service.interfaces.ISaludoService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Data
@RestController
@RequestMapping("/api/greeting")
public class SaludoController {
    private ISaludoService saludoService;
    /* Endpoint con saludo a mostrar
    @GetMapping("/hello") */
    @PostMapping("/hello")
    public ResponseEntity<String> findInitGreeting() {
        return new ResponseEntity<>("Hola, este es mi primer hola mundo peter anto...", HttpStatus.OK);
    }
    @GetMapping("/hello/{id}")
    public ResponseEntity<String> findInitGreetingVariable(@PathVariable("id") Long aVariable) {
        return new ResponseEntity<>("Hola, este es mi primer hola mundo, el valor variable es: " +
                aVariable, HttpStatus.OK);
    }

/* endpoint con parameters */
    @GetMapping("/hello/parameters")
    public ResponseEntity<String> findInitGreetingWithParameters(@RequestParam("name") String aName , @RequestParam("apellido") String aApellido)
    {
        return new ResponseEntity<>("Hola, este es mi primer hola mundo, nombres recibidos por url con parameters pn: " + aName+" pa:"+aApellido,
                HttpStatus.OK);
    }


    /**
     * Endpointsaludo de acuerdo a un número
     */
    @GetMapping("/hello/according/number/{id}")
    public ResponseEntity<String> findIAccordingNumber(@PathVariable("id") Long aNumber) {
        return new ResponseEntity<>(this.saludoService.greetingAccordingNumber(aNumber),
                HttpStatus.OK);
    }
    //////antes esta la declaración de la clase…//////
//public class SaludoController{
//atributo

    //vienen todos los endpoints
//ahora si el setter

////// } cierra la clase //////
@Autowired
@Qualifier("saludoUrgenteServiceImpl")
public void setSaludoService(ISaludoService saludoService) {
        this.saludoService = saludoService;
    }



}
