package co.uis.edu.onboarding.repository;

import co.uis.edu.onboarding.model.Mensaje;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// es esta capa van todas las consultas
@Repository
public interface IMensajeRepository extends JpaRepository<Mensaje,Long> {
}
