package co.uis.edu.onboarding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;



@SpringBootApplication
public class OnboardingApplication  extends SpringBootServletInitializer {
	
	
	/**
	 * Permite que el contexto de wildfly reconozca los endpoints
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(OnboardingApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(OnboardingApplication.class, args);
	}

}
